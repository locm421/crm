package crm_project_22.api;

import java.io.IOException;
import java.io.PrintWriter;

import com.google.gson.Gson;

import crm_project_22.playload.response.BaseResponse;
import crm_project_22.service.GroupworkService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(name = "apiGroupworkController", urlPatterns = {"/api/groupworkapi/delete"})
public class ApiGroupworkController extends HttpServlet{
	private Gson gson = new Gson() ;
	GroupworkService grsv = new GroupworkService();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int id = Integer.parseInt(req.getParameter("id"));
		boolean isSuccess = grsv.delete(id);
		BaseResponse response = new BaseResponse();
		response.setStatusCode(200);
		response.setMessage(isSuccess ? "Xóa thành công" : "Xóa thất bại");
		response.setData(isSuccess);
		String json = gson.toJson(response);
		PrintWriter out = resp.getWriter();
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		out.print(json);
		out.flush();
	}
}
