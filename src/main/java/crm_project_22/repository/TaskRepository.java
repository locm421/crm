package crm_project_22.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import crm_project_22.config.MysqlConfig;
import crm_project_22.entity.LoaiThanhVien;
import crm_project_22.entity.NguoiDung;
import crm_project_22.entity.Task;

public class TaskRepository {
	public List<Task> getAllTask(){
		String query ="SELECT \r\n"
				+ "	CV.id AS 'ID_CV',\r\n"
				+ "    CV.ten AS 'Ten_CV',\r\n"
				+ "    DA.ten AS 'Ten_DA',\r\n"
				+ "    ND.fullname AS 'NTH',\r\n"
				+ "    CV.ngayBatDau AS 'NBD',\r\n"
				+ "    CV.ngayKetThuc AS 'NKT',\r\n"
				+ "    TT.ten AS 'TT'\r\n"
				+ "FROM CongViec CV\r\n"
				+ "INNER JOIN DuAn DA ON CV.id_DuAn = DA.id\r\n"
				+ "INNER JOIN CongViec_NguoiDung CVND ON CV.id = CVND.id_CongViec\r\n"
				+ "INNER JOIN NguoiDung ND ON CVND.id_NguoiDung = ND.id\r\n"
				+ "INNER JOIN TrangThai TT ON CV.id_TrangThai = TT.id;";
		Connection connection = MysqlConfig.getConnection();
		List<Task> listTask = new ArrayList<Task>();
		try {
			PreparedStatement statement = connection.prepareStatement(query);
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				Task task = new Task();
				task.setId(resultSet.getInt("ID_CV"));
				task.setTen(resultSet.getString("Ten_CV"));
				task.setdA(resultSet.getString("Ten_DA"));
				task.setNTH(resultSet.getString("NTH"));
				task.setNgayBatDau(resultSet.getString("NBD"));
				task.setNgayKetThuc(resultSet.getString("NKT"));
				task.setTrangThai(resultSet.getString("TT"));
				listTask.add(task);
				for (Task taskf : listTask) {
					System.out.println(taskf.getId());
					System.out.println(taskf.getTen());
					System.out.println(taskf.getdA());
					System.out.println(taskf.getNgayBatDau());
					System.out.println(taskf.getTrangThai());
				}
			}
			
		} catch (SQLException e) {
			System.out.println("Lỗi thực thi câu query" + e.getLocalizedMessage());
		}finally {
			if(connection != null)
			{
				try {
					connection.close();
				} catch (Exception e) {
					System.out.println("Lỗi đóng kết nối" + e.getLocalizedMessage());
				}
			}
			return listTask;
		}
		
	}
	public int insert(String ten, String ngBD, String ngKT) {
		int count = 0;
		SimpleDateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
		String query = "INSERT INTO DuAn(ten,ngayBatDau,ngayKetThuc) VALUES(?,?,?)";
		Connection connection = MysqlConfig.getConnection();
		
			try {
				Date dateBD = inputFormat.parse(ngBD);
				Date dateKT = inputFormat.parse(ngKT);
				String formattedbd = outputFormat.format(dateBD);
				String formattedkt = outputFormat.format(dateKT);
				PreparedStatement statement = connection.prepareStatement(query);
				System.out.println(formattedbd);
				System.out.println(formattedkt);
			statement.setString(1, ten);
			statement.setString(2, formattedbd);
			statement.setString(3, formattedkt);
			count = statement.executeUpdate();
			} catch (ParseException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   
		return count;    
	}
}
