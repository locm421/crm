package crm_project_22.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import crm_project_22.config.MysqlConfig;
import crm_project_22.entity.LoaiThanhVien;
import crm_project_22.entity.NguoiDung;

public class NguoiDungRepository {
	public List<NguoiDung> findByEmailAndPassword(String email, String matkhau){
		String query ="SELECT * FROM NguoiDung nd JOIN LoaiThanhVien ltv ON ltv.id = nd.id_loaithanhvien WHERE nd.email = ? AND nd.matkhau = ?";
		Connection connection = MysqlConfig.getConnection();
		List<NguoiDung> listNguoiDung = new ArrayList<NguoiDung>();
		try {
			PreparedStatement statement = connection.prepareStatement(query);
			statement.setString(1, email);
			statement.setString(2, matkhau);
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				NguoiDung nguoiDung = new NguoiDung();
				nguoiDung.setId(resultSet.getInt("id"));
				nguoiDung.setFullname(resultSet.getString("fullname"));
				nguoiDung.setEmail(resultSet.getString("email"));
				LoaiThanhVien loaiThanhVien = new LoaiThanhVien();
				loaiThanhVien.setTen(resultSet.getString("ten"));
				nguoiDung.setLoaiThanhVien(loaiThanhVien);
				listNguoiDung.add(nguoiDung);
			}
			
		} catch (SQLException e) {
			System.out.println("Lỗi thực thi câu query" + e.getLocalizedMessage());
		}finally {
			if(connection != null)
			{
				try {
					connection.close();
				} catch (Exception e) {
					System.out.println("Lỗi đóng kết nối" + e.getLocalizedMessage());
				}
			}
			return listNguoiDung;
		}
		
	}
	public int deleteUser(int id) {
		int count = 0;
		String query = "DELETE FROM NguoiDung nd WHERE nd.id = ?";
		Connection connection = MysqlConfig.getConnection();
		try {
			PreparedStatement statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			count = statement.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return count;
	}
	public int insert(String fullname, String email, String password, String phoneNo, String country, int role) {
		int count = 0;
		String query = "INSERT INTO NguoiDung(fullname,email,matkhau,diachi,soDienThoai,id_loaithanhvien) VALUES(?,?,?,?,?,?)";
		Connection connection = MysqlConfig.getConnection();
		try {
			PreparedStatement statement = connection.prepareStatement(query);
			statement.setString(1, fullname);
			statement.setString(2, email);
			statement.setString(3, password);
			statement.setString(4, country);
			statement.setString(5, phoneNo);
			statement.setInt(6, role);
			count = statement.executeUpdate();
		}catch(SQLException e){
			System.out.println("Lỗi thêm dữ liệu " + e.getLocalizedMessage());
		}
		
		
		return count;
	}
	public List<NguoiDung> getAllNguoiDung(){
		List<NguoiDung> list = new ArrayList<NguoiDung>();
		String query = "SELECT * FROM NguoiDung nd JOIN LoaiThanhVien ltv ON ltv.id = nd.id_loaithanhvien";
		Connection connection = MysqlConfig.getConnection();
		try {
			PreparedStatement statement = connection.prepareStatement(query);
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				NguoiDung user = new NguoiDung();
				LoaiThanhVien ltv = new LoaiThanhVien();
				user.setId(resultSet.getInt("id"));
				user.setFullname(resultSet.getString("fullname"));
				user.setEmail(resultSet.getString("email"));
				user.setMatkhau(resultSet.getString("matkhau"));
				user.setDiaChi(resultSet.getString("diachi"));
				user.setSoDienThoai(resultSet.getString("soDienThoai"));
				ltv.setTen(resultSet.getString("ten"));
				user.setLoaiThanhVien(ltv);
				user.setRole(ltv.getTen());
				list.add(user);
				System.out.println(user.getFullname());
				System.out.println(user.getDiaChi());
				System.out.println(user.getLoaiThanhVien().getTen());
				System.out.println(user.getLoaiThanhVien().getTen());
			}
			
		}catch(SQLException e)
		{
			System.out.println("Lỗi kết nối " + e.getLocalizedMessage());
		}finally {
			try {
				connection.close();
			} catch (Exception e2) {
				System.out.println("Lỗi đóng kết nối " + e2.getLocalizedMessage());
			}
		}
		return list;
	}
}
