package crm_project_22.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.taglibs.standard.tag.el.fmt.ParseDateTag;

import com.mysql.cj.xdevapi.Result;

import crm_project_22.config.MysqlConfig;
import crm_project_22.entity.DuAn;
import java.text.DateFormat; 
import java.text.ParseException; 
import java.text.SimpleDateFormat; 
import java.util.Date;
public class GroupworkReponsitory {
	public List<DuAn> getAllDuAn(){
		List<DuAn> list = new ArrayList<DuAn>();
		String query = "SELECT * FROM DuAn da";
		Connection connection = MysqlConfig.getConnection();
		try {
			PreparedStatement statement = connection.prepareStatement(query);
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				DuAn duAn = new DuAn();
				duAn.setId(resultSet.getInt("id"));
				duAn.setTen(resultSet.getString("ten"));
				duAn.setNgayBatDau(resultSet.getString("ngayBatDau"));
				duAn.setNgayKetThuc(resultSet.getString("ngayKetThuc"));
				list.add(duAn);
				System.out.println(duAn.getId());
				System.out.println(duAn.getNgayBatDau());
				System.out.println(duAn.getNgayKetThuc());
			}
		} catch (Exception e) {
			System.out.println("Lỗi kết nối cơ sỡ dữ liệu " + e.getLocalizedMessage());
		}finally {
		try {
			connection.close();
		} catch (Exception e2) {
			System.out.println("Lỗi kết nối cơ sỡ dữ liệu " + e2.getLocalizedMessage());
			
		}
		return list;
		}
	}
	public int insert(String ten, String ngBD, String ngKT) {
		int count = 0;
		SimpleDateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
		String query = "INSERT INTO DuAn(ten,ngayBatDau,ngayKetThuc) VALUES(?,?,?)";
		Connection connection = MysqlConfig.getConnection();
		
			try {
				Date dateBD = inputFormat.parse(ngBD);
				Date dateKT = inputFormat.parse(ngKT);
				String formattedbd = outputFormat.format(dateBD);
				String formattedkt = outputFormat.format(dateKT);
				PreparedStatement statement = connection.prepareStatement(query);
				System.out.println(formattedbd);
				System.out.println(formattedkt);
			statement.setString(1, ten);
			statement.setString(2, formattedbd);
			statement.setString(3, formattedkt);
			count = statement.executeUpdate();
			} catch (ParseException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   
		return count;    
	}
	public int deleteDuAn(int id) {
		int count = 0;
		String query = "DELETE FROM DuAn da WHERE da.id = ?";
		Connection connection = MysqlConfig.getConnection();
		try {
			PreparedStatement statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			count = statement.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return count;
	}
}
