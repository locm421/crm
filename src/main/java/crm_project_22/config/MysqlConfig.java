package crm_project_22.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlConfig {
	//Tạo kết nối tới CSDL
	public static Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3307/crm","root" ,"mypassword");
		} catch (ClassNotFoundException  |SQLException e ) {
			// TODO Auto-generated catch block
			System.out.println("Lỗi kết nối CSDL" + e.getLocalizedMessage());
		}
		return connection;
	}
}
