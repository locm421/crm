package crm_project_22.entity;

public class Task {
	private int id;
	private String ten;
	private String NTH;
	private String dA;
	private String ngayBatDau;
	private String ngayKetThuc;
	private String trangThai;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public String getNTH() {
		return NTH;
	}
	public void setNTH(String nTH) {
		NTH = nTH;
	}
	public String getdA() {
		return dA;
	}
	public void setdA(String dA) {
		this.dA = dA;
	}
	public String getNgayBatDau() {
		return ngayBatDau;
	}
	public void setNgayBatDau(String ngayBatDau) {
		this.ngayBatDau = ngayBatDau;
	}
	public String getNgayKetThuc() {
		return ngayKetThuc;
	}
	public void setNgayKetThuc(String ngayKetThuc) {
		this.ngayKetThuc = ngayKetThuc;
	}
	public String getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}
	
	
}
