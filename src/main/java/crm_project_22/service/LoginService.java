package crm_project_22.service;

import java.util.List;

import crm_project_22.entity.NguoiDung;
import crm_project_22.repository.NguoiDungRepository;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

public class LoginService {
	private NguoiDungRepository nguoiDungRepository = new NguoiDungRepository();
	public boolean checkLogin(HttpServletRequest request,String email, String password) {
		List<NguoiDung> listNguoiDung = new NguoiDungRepository().findByEmailAndPassword(email, password);
		if(listNguoiDung.size()>0)
		{
		HttpSession session = request.getSession();

		session.setAttribute("roleName", listNguoiDung.get(0).getLoaiThanhVien().getTen());
		}
		return listNguoiDung.size() > 0;
	}
	
}
