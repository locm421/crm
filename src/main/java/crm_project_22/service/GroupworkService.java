package crm_project_22.service;

import java.util.List;

import crm_project_22.entity.DuAn;
import crm_project_22.repository.GroupworkReponsitory;

public class GroupworkService {
	GroupworkReponsitory grr = new GroupworkReponsitory();
	public List<DuAn> getAllDuAn(){
		return grr.getAllDuAn();
	}
	public boolean insert (String ten, String ngayBD, String ngayKT) {
		int count = grr.insert(ten, ngayBD, ngayKT);
		return count > 0;
	}
	public boolean delete(int id) {
		int count = grr.deleteDuAn(id);
		return count > 0;
	}
}
