package crm_project_22.service;

import java.util.List;

import crm_project_22.entity.NguoiDung;
import crm_project_22.repository.NguoiDungRepository;

public class UserService {
	private NguoiDungRepository nguoiDungRepository = new NguoiDungRepository();
	public boolean insert (String fullname, String email, String password, String phoneNo, String country, int role) {
		int count = nguoiDungRepository.insert(fullname, email, password, phoneNo, country, role);
		return count > 0;
	}
	public boolean delete(int id) {
		int count = nguoiDungRepository.deleteUser(id);
		return count > 0;
	}
	public  List<NguoiDung> getAllNguoiDung(){
		return nguoiDungRepository.getAllNguoiDung();
	}
}
