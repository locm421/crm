package crm_project_22.controller;

import java.io.IOException;

import crm_project_22.service.GroupworkService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(name="groupworkController", urlPatterns = {"/groupwork","/groupwork-add", "/groupwork-details"})
public class GroupworkController extends HttpServlet {
	private GroupworkService grsv = new GroupworkService();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pathInfo = req.getServletPath();
        switch (pathInfo) {
		case "/groupwork":
			req.setAttribute("listDuAn", grsv.getAllDuAn());
			req.getRequestDispatcher("groupwork.jsp").forward(req, resp);
			break;
		case "/groupwork-add":
			
			req.getRequestDispatcher("groupwork-add.jsp").forward(req, resp);
			break;
		case "/groupwork-details":
			req.getRequestDispatcher("groupwork-details.jsp").forward(req, resp);
			break;
		default:
			break;
		}
	}
		
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pathInfo = req.getServletPath();
        switch (pathInfo) {
		case "/groupwork":
			req.getRequestDispatcher("groupwork.jsp").forward(req, resp);
			break;
		case "/groupwork-add":
		String ten = req.getParameter("ten");
		String ngayBD = req.getParameter("ngayBD");
		String ngayKT = req.getParameter("ngayKT");
		boolean isSuccessGR = grsv.insert(ten, ngayBD, ngayKT);
		req.setAttribute("isSuccessGR",isSuccessGR);
		req.getRequestDispatcher("groupwork-add.jsp").forward(req, resp);
			break;
		default:
			break;
		}
		
	}
}
