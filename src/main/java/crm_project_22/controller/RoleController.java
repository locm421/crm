package crm_project_22.controller;

import java.io.IOException;

import crm_project_22.service.RoleService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet(name = "roleController",urlPatterns = {"/role-table","/role-add"})
public class RoleController extends HttpServlet{
	private RoleService roleService = new RoleService();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pathInfo = req.getServletPath();
        switch (pathInfo) {
		case "/role-table":
			req.setAttribute("listRole", roleService.getAllLoaiThanhVien());
			req.getRequestDispatcher("role-table.jsp").forward(req, resp);
			break;
		case "/role-add":
			req.getRequestDispatcher("role-add.jsp").forward(req, resp);
			break;
		default:
			break;
		}
	}
		
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pathInfo = req.getServletPath();
        switch (pathInfo) {
		case "/role-table":
			req.getRequestDispatcher("role-table.jsp").forward(req, resp);
			break;
		case "/role-add":
		String tenQuyen = req.getParameter("tenQuyen");
		String mota = req.getParameter("moTa");
		boolean isSuccess = roleService.insert(tenQuyen, mota);
		req.setAttribute("isSucess", isSuccess);

		req.getRequestDispatcher("role-add.jsp").forward(req, resp);
			break;
		default:
			break;
		}
		
	}
}
