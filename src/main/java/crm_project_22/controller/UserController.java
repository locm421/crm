package crm_project_22.controller;

import java.io.IOException;

import com.mysql.cj.x.protobuf.MysqlxCrud.Insert;

import crm_project_22.entity.NguoiDung;
import crm_project_22.service.UserService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(name = "UserController", urlPatterns = {"/user-table","/user-add"})
public class UserController extends HttpServlet{
	private UserService user = new UserService();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String path = req.getServletPath();
		switch(path) {
		case "/user-table":
			req.setAttribute("listUser", user.getAllNguoiDung());
			req.getRequestDispatcher("user-table.jsp").forward(req, resp);
			req.getRequestDispatcher("task-add.jsp").forward(req, resp);
			break;
		case "/user-add":
			req.getRequestDispatcher("user-add.jsp").forward(req, resp);
			break;
		default:
			break;
		}
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		NguoiDung nguoiDung = new NguoiDung();
		String pathInfo = req.getServletPath();
		switch(pathInfo) {
		case "/user-table":
			req.getRequestDispatcher("user-table.jsp").forward(req, resp);
			break;
		case "/user-add":
			String fullname = req.getParameter("fullname");
			String email = req.getParameter("example-email");
			String password = req.getParameter("password");
			String phoneNo = req.getParameter("phoneNo");
			String country = req.getParameter("country");
			int role = Integer.parseInt(req.getParameter("role"));
			boolean isSuscessUser = user.insert(fullname, email, password, phoneNo, country, role);
			req.setAttribute("isSuscessUser",isSuscessUser);
			req.getRequestDispatcher("user-add.jsp").forward(req, resp);
			break;
		default:
			break;
		}
		
	}
}
