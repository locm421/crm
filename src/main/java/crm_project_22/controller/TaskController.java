package crm_project_22.controller;

import java.io.IOException;

import crm_project_22.entity.NguoiDung;
import crm_project_22.entity.Task;
import crm_project_22.service.GroupworkService;
import crm_project_22.service.TaskService;
import crm_project_22.service.UserService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(name="taskController", urlPatterns = {"/task", "/task-add"})
public class TaskController extends HttpServlet {
	private TaskService taskSV = new TaskService();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UserService user = new UserService();
		GroupworkService grsv = new GroupworkService();
		String path = req.getServletPath();
		switch(path) {
		case "/task":
			req.setAttribute("listTask", taskSV.getAllTask() );	
			req.getRequestDispatcher("task.jsp").forward(req, resp);
			break;
		case "/task-add":
			req.setAttribute("listDuAn", grsv.getAllDuAn());
			req.setAttribute("listUser", user.getAllNguoiDung());
			for (NguoiDung us : user.getAllNguoiDung()) {
				System.out.println(us.getFullname());
			}
			
			req.getRequestDispatcher("task-add.jsp").forward(req, resp);
			break;
		default:
			break;
		}
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String pathInfo = req.getServletPath();
		switch(pathInfo) {
		case "/task":
			req.getRequestDispatcher("task.jsp").forward(req, resp);
			break;
		case "/user-add":
			String id_DuAn = req.getParameter("id_DuAn");
			String ngayBD = req.getParameter("ngayBD");
			System.out.println("oooooooooooooo");
			req.getRequestDispatcher("task-add.jsp").forward(req, resp);
			break;
		default:
			break;
		}
		
	}
}
