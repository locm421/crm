package crm_project_22.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.cj.xdevapi.Result;

import crm_project_22.config.MysqlConfig;
import crm_project_22.entity.NguoiDung;
import crm_project_22.service.LoginService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet(name = "loginController", urlPatterns = {"/login"})
public class LoginController extends HttpServlet {
	private LoginService loginService = new LoginService();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("login.jsp").forward(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String email = req.getParameter("email");
		String matkhau = req.getParameter("password");
		
	
		boolean isSuccess = loginService.checkLogin(req, email, matkhau);
		if(isSuccess)
		{
			System.out.println("Dang nhap THANH CONG");
			req.getRequestDispatcher("index.html").forward(req, resp);
		}else {
			System.out.println("Đăng nhập THAT BAI");
		}
		
	}
}
