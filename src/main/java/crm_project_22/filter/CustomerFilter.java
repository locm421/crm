package crm_project_22.filter;

import java.io.IOException;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
 
@WebFilter(filterName="customerFilter", urlPatterns = {"/role-add", "/user-add","/groupwork-add"})
public class CustomerFilter implements Filter {
int i = 0;
	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		
		
		HttpServletRequest servletRequest = (HttpServletRequest)arg0;
		HttpServletResponse servletResponse = (HttpServletResponse) arg1;
		String path = servletRequest.getServletPath();
		HttpSession session = servletRequest.getSession();
		String roleName = (String) session.getAttribute("roleName");
		System.out.println(roleName);
		System.out.println(path);
		switch (path) {
		case "/role-add":
			if(roleName != null&& roleName.toUpperCase().equals("ADMIN"))
			{
				arg2.doFilter(arg0, arg1);
			}else
			{
				servletResponse.sendRedirect(servletRequest.getContextPath() + "/");
			
			}
			break;
		case "/groupwork-add":
			if(roleName != null&& roleName.toUpperCase().equals("ADMIN"))
			{
				arg2.doFilter(arg0, arg1);
			}else
			{
				servletResponse.sendRedirect(servletRequest.getContextPath() + "/");
			
			}
			break;
		case "/user-add":
			if(roleName != null&& roleName.toUpperCase().equals("ADMIN"))
			{
				arg2.doFilter(arg0, arg1);
			}else
			{
				servletResponse.sendRedirect(servletRequest.getContextPath() + "/");
			
			}
			break;

		default:
			break;
		}
		
	}
	}
	


